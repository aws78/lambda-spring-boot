package suma;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import suma.model.Request;
import suma.model.Response;

/**
 * Handler for requests to Lambda function.
 */
public class App implements RequestHandler<Request, Response> {


    @Override
    public Response handleRequest(Request request, Context i){
        Response response = new Response(request.getNumberA() + request.getNumberB());
        return response;
    }
}