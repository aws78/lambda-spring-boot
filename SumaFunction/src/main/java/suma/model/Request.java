package suma.model;

public class Request {

    private Integer numberA = 0;
    private Integer numberB = 0;

    public Request() {
    }

    public Request(Integer numberA, Integer numberB) {
        this.numberA = numberA;
        this.numberB = numberB;
    }

    public Integer getNumberA() {
        return numberA;
    }

    public void setNumberA(Integer numberA) {
        this.numberA = numberA;
    }

    public Integer getNumberB() {
        return numberB;
    }

    public void setNumberB(Integer numberB) {
        this.numberB = numberB;
    }
}
